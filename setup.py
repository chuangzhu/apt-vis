#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='aptvis',
      version='0.2.8',
      description='Visualize Debian local package database,' +
                  ' a fork of farseerfc/pacvis',
      author='Zhu Chuang',
      author_email='genelocated@yandex.com',
      url='https://gitlab.com/genelocated/aptvis',
      packages=find_packages(),
      install_requires=["tornado"],
      package_data={'aptvis': ['templates/index.template.html',
                               'static/*'
                               ]},
      entry_points={
          'console_scripts': ['aptvis = aptvis.aptvis:main']
      },
      )
