# AptVis

Tool for visualizing your installed packages on your Debian and Debian-based (Ubuntu etc.) machines.

### [Try it with your browser](https://aptvis.melty.land)

![full](https://img.vim-cn.com/c6/1fd8f22b8e20b6132ad9493cd0cd6f5ccec2bb.png)
![zoomin](https://img.vim-cn.com/0e/5c35b5b0104def4b0de547cd30d57656041ae2.png)

This is a fork of [farseerfc/pacvis](https://farseerfc.me/en/pacvis.html), which is originally designed for Arch Linux.

* [My blog post in Chinese](https://melty.land/blog/apt-vis)

## Installation

[Download the latest package from here](https://gitlab.com/genelocated/apt-vis/tags), then install it using APT:

```bash
sudo apt install ~/path/to/aptvis_0.2.7-1_all.deb
```

## Usage

```bash
aptvis
```

Then go to http://localhost:8888/ .

## Development

Dependencies:

* `python3-apt`
* `python3-tornado`
* `python3-pkg-resources`

Running from source:

```bash
git clone https://gitlab.com/genelocated/apt-vis.git aptvis
cd aptvis
python3 -m aptvis.aptvis
```

To make a Debian package, install dependencies above, `python3-setuptools` and `devscripts`, then run:

```bash
git clone https://gitlab.com/genelocated/apt-vis.git aptvis
tar -cz aptvis -f aptvis_<version>.orig.tar.gz
cd aptvis
debuild
```

